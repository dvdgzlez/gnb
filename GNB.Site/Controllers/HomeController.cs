﻿using GNB.DataClient;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace GNB.Site.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetRates(string token)
        {
            return Content(JsonConvert.SerializeObject(RatesClient.Get(token).Result), "application/json");
        }

        public ActionResult GetTransactions(string token)
        {
            return Content(JsonConvert.SerializeObject(TransactionsClient.Get(token).Result), "application/json");
        }

        public ActionResult GetTransactionsBySku(string token, string sku, string currency = "EUR")
        {
            return Content(JsonConvert.SerializeObject(TransactionsClient.GetBySku(token, sku, currency).Result), "application/json");
        }
    }
}
