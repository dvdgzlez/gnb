﻿using GNB.Utils.Constants;
using GNB.Utils.Models;
using GNB.Utils.RequestHelper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.DataClient
{
    public class TransactionsClient
    {
        public static async Task<List<Transaction>> Get(string token) =>
            await RequestHelper<List<Transaction>>.GetRequest(Connection.ApiTransactionGet(token));

        public static async Task<List<Transaction>> GetBySku(string token, string sku, string currency = "EUR") =>
            await RequestHelper<List<Transaction>>.GetRequest(Connection.ApiTransactionGetBySku(token, sku, currency));
    }
}
