﻿using GNB.Utils.Constants;
using GNB.Utils.Models;
using GNB.Utils.RequestHelper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.DataClient
{
    public class RatesClient
    {
        public static async Task<List<Rate>> Get(string token) =>
            await RequestHelper<List<Rate>>.GetRequest(Connection.ApiRateGet(token));
    }
}
