﻿using GNB.Utils.Interfaces;
using Newtonsoft.Json;
using System.IO;

namespace GNB.Utils.FileStorage
{
    public abstract class FileStorage<T>
    {
        private readonly string _filePath;
        protected string token;
        public FileStorage(string filePath)
        {
            _filePath = $@"C:\GNB\{filePath}";
            if (!Directory.Exists(@"C:\GNB"))
            {
                Directory.CreateDirectory(@"C:\GNB");
            }
        }

        protected T Read()
        {
            TextReader reader = null;
            try
            {
                string fullPath = _filePath + token + ".txt";
                if (File.Exists(fullPath))
                {
                    reader = new StreamReader(fullPath);
                    var fileContents = reader.ReadToEnd();
                    return JsonConvert.DeserializeObject<T>(fileContents);
                }
                else
                {
                    return default(T);
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        protected void Save(T data)
        {
            TextWriter writer = null;
            try
            {
                string fullPath = _filePath + token + ".txt";
                var contentsToWriteToFile = JsonConvert.SerializeObject(data);
                writer = new StreamWriter(fullPath, true);
                writer.Write(contentsToWriteToFile);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }
    }
}
