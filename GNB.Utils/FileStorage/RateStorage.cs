﻿using GNB.Utils.Interfaces;
using GNB.Utils.Models;
using System.Collections.Generic;

namespace GNB.Utils.FileStorage
{
    public class RateStorage : FileStorage<List<Rate>>, IStorage<List<Rate>>
    {
        public RateStorage() : base("rates")
        {
        }

        public void Add(List<Rate> data)
        {
            Save(data);
        }

        public List<Rate> Get()
        {
            return Read();
        }

        public IStorage<List<Rate>> AddToken(string _token)
        {
            token = _token;
            return this;
        }
    }
}
