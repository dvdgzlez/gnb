﻿using GNB.Utils.Interfaces;
using GNB.Utils.Models;
using System.Collections.Generic;

namespace GNB.Utils.FileStorage
{
    public partial class TransactionsStorage : FileStorage<List<Transaction>>, IStorage<List<Transaction>>
    {
        public TransactionsStorage() : base("transactions")
        {
        }

        public void Add(List<Transaction> data)
        {
            new TransactionsStorage().Save(data);
        }

        public List<Transaction> Get()
        {
            return new TransactionsStorage().Read();
        }

        public IStorage<List<Transaction>> AddToken(string _token)
        {
            token = _token;
            return this;
        }
    }
}
