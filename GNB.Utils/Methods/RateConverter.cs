﻿using GNB.Utils.Interfaces;
using GNB.Utils.Models;
using System.Collections.Generic;
using System.Linq;

namespace GNB.Utils.Methods
{
    public class RateConverter : IConverter<Rate, Transaction>
    {
        private List<Rate> _rates;

        public RateConverter(List<Rate> rates)
        {
            _rates = rates;
        }

        public string ConvertValue(Transaction value, string to)
        {
            if (!_rates.Exists(r => r.from == value.currency && r.to == to))
            {
                if (!CreateConverter(new Rate { from = value.currency, to = to }))
                {
                    return "";
                }
            }

            var rate = _rates.Find(r => r.from == value.currency && r.to == to);
            return Calculator.Multiply(value.amount, rate.rate);
        }

        public bool CreateConverter(Rate value)
        {
            if (string.IsNullOrEmpty(value.from) ||
                string.IsNullOrEmpty(value.to))
            {
                return false;
            }
            var listFrom = _rates.Where(rate => rate.from == value.from).ToList();
            var listTo = _rates.Where(rate => rate.to == value.to).ToList();
            listFrom.ForEach(f =>
            {
                if (listTo.Exists(t => f.to == t.from))
                {
                    var t = listTo.Find(x => f.to == x.from);
                    value.rate = Calculator.Multiply(f.rate, t.rate);
                }
            });
            if (string.IsNullOrEmpty(value.rate))
            {
                return false;
            }
            else
            {
                _rates.Add(value);
            }
            return true;
        }
    }
}
