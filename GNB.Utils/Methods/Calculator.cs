﻿using System;

namespace GNB.Utils.Methods
{
    public class Calculator
    {
        public static string Multiply(string value1, string value2)
        {
            double result = Convert.ToDouble(value1) * Convert.ToDouble(value2);
            return string.Format("{0:N2}", result);
        }
    }
}
