﻿using GNB.Utils.Constants;
using GNB.Utils.DataClient;
using GNB.Utils.Interfaces;
using GNB.Utils.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Utils.Management
{
    public class RateManagement
    {
        public IStorage<List<Rate>> _storage;

        public RateManagement(IStorage<List<Rate>> storage)
        {
            _storage = storage;
        }
        public List<Rate> Get(string token)
        {
            using (RatesClient client = new RatesClient())
            {
                try
                {
                    var request = Task.Run(async () => await client.GetRequest(Connection.RateJson)).Result;
                    _storage.AddToken(token).Add(request);
                    return request;
                }
                catch (Exception e)
                {
                    return _storage.AddToken(token).Get();
                }
            }
        }
    }
}
