﻿using GNB.Utils.Constants;
using GNB.Utils.DataClient;
using GNB.Utils.FileStorage;
using GNB.Utils.Interfaces;
using GNB.Utils.Methods;
using GNB.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNB.Utils.Management
{
    public class TransactionManagement
    {
        public IConverter<Rate, Transaction> _converter;
        public IStorage<List<Transaction>> _storage;

        public TransactionManagement(IStorage<List<Transaction>> storage)
        {
            _storage = storage;
        }

        public List<Transaction> Get(string token)
        {
            using (TransactionsClient client = new TransactionsClient())
            {
                try
                {
                    var request = Task.Run(async () => await client.GetRequest(Connection.TransactionJson)).Result;
                    _storage.Add(request);
                    return request;
                }
                catch (Exception)
                {
                    return _storage.Get();
                }
            }
        } 

        public Response GetBySKU(string token, string sku, string currency)
        {
            var transactions = new TransactionsStorage().AddToken(token).Get();
            var rates = new RateStorage().AddToken(token).Get();

            if (transactions == null || rates == null)
            {
                return new Response(new List<Transaction>(), 0);
            }

            var newTransactions = transactions.Where(t => t.sku == sku).ToList();
            _converter = new RateConverter(rates);
            double total = 0;
            newTransactions.ForEach(transaction =>
            {
                if (transaction.currency != currency)
                {
                    transaction.amount = _converter.ConvertValue(transaction, currency);
                    transaction.currency = currency;
                }
                var converted = Convert.ToDouble(transaction.amount);
                total += converted;
            });

            return new Response(newTransactions, total);
        }
    }
}
