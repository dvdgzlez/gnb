﻿namespace GNB.Utils.Enum
{
    public enum HttpMethodEnum
    {
        Get = 1,
        Post = 2,
        Put = 3,
        Delete = 4
    }
}
