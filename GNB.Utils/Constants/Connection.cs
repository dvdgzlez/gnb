﻿namespace GNB.Utils.Constants
{
    public class Connection
    {
        public static string RateJson => "http://quiet-stone-2094.herokuapp.com/rates.json";
        public static string TransactionJson => "http://quiet-stone-2094.herokuapp.com/transactions.json";

        //API
        public static string ApiRateGet(string token) => $"http://localhost:51982/api/rates/get?token={token}";
        public static string ApiTransactionGet(string token) => $"http://localhost:51982/api/transactions/get?token={token}";
        public static string ApiTransactionGetBySku(string token, string sku, string currency = "EUR") => $"http://localhost:51982/api/transactions/getbysku?token={token}&sku=${sku}&currency=${currency}";
    }
}
