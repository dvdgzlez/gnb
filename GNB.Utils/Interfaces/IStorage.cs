﻿namespace GNB.Utils.Interfaces
{
    public interface IStorage<T>
    {
        void Add(T data);
        T Get();
        IStorage<T> AddToken(string token);
    }
}
