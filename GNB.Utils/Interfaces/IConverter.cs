﻿namespace GNB.Utils.Interfaces
{
    public interface IConverter<T, TR>
    {
        string ConvertValue(TR value, string to);
        bool CreateConverter(T value);
    }
}
