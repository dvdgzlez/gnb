﻿using GNB.Utils.Enum;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GNB.Utils.RequestHelper
{
    public class RequestHelper<TR>
    {
        private static bool _hasContent;

        public static async Task<TR> GetRequest(string url)
        {
            _hasContent = false;
            return await BaseRequest(url, HttpMethodEnum.Get, false);
        }

        public static async Task<TR> PostRequest<T>(T payload, string url)
        {
            _hasContent = true;
            return await BaseRequest(url, HttpMethodEnum.Post, payload);
        }

        public static async Task<TR> PostRequest(string url)
        {
            _hasContent = false;
            return await BaseRequest(url, HttpMethodEnum.Post, false);
        }

        public static async Task<TR> PutRequest<T>(T payload, string url)
        {
            _hasContent = true;
            return await BaseRequest(url, HttpMethodEnum.Put, payload);
        }

        public static async Task<TR> PutRequest(string url)
        {
            _hasContent = false;
            return await BaseRequest(url, HttpMethodEnum.Put, false);
        }

        public static async Task<TR> DeleteRequest(string url)
        {
            _hasContent = false;
            return await BaseRequest(url, HttpMethodEnum.Delete, false);
        }

        private static async Task<TR> BaseRequest<T>(string url, HttpMethodEnum method, T payload)
        {
            try
            {
#if _net_core_
                var httpHandler = new HttpClientHandler();
                httpHandler.ServerCertificateCustomValidationCallback += (message, cert, chain, errors) => true;
                var client = new HttpClient(httpHandler);
#else
                var client = new HttpClient();
#endif
                StringContent content = null;

                if (_hasContent)
                {
                    client.DefaultRequestHeaders.ConnectionClose = true;
                    content = new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json");
                }

                Console.WriteLine($"Requesting [{url}]");

                HttpResponseMessage response;

                switch (method)
                {
                    case HttpMethodEnum.Get:
                        response = await client.GetAsync(url);
                        break;
                    case HttpMethodEnum.Post:
                        response = await client.PostAsync(url, content);
                        break;
                    case HttpMethodEnum.Put:
                        response = await client.PutAsync(url, content);
                        break;
                    case HttpMethodEnum.Delete:
                        response = await client.DeleteAsync(url);
                        break;
                    default:
                        return default(TR);
                }

                client.Dispose();
                client = null;

                if (response.IsSuccessStatusCode)
                {

                    string responseContent = await response.Content.ReadAsStringAsync();
                    if (responseContent.Contains("GenericDataResponse"))
                    {
                        var resultWeappedObject =
                            JsonConvert.DeserializeObject<TR>(responseContent);
                        return resultWeappedObject;
                    }
                    else
                    {
                        var resultObject = JsonConvert.DeserializeObject<TR>(responseContent);
                        return resultObject;
                    }
                }
                else
                {
                    throw new Exception($"{url} ({response.ReasonPhrase})");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"RequestHelper Exception {ex}");
            }
        }

    }
}
