﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace GNB.Utils.DataClient
{
    public abstract class DataClient<T> : IDisposable where T : class
    {
        private HttpClient _client;
        public DataClient()
        {
            _client = new HttpClient();
        }

        public async Task<T> GetRequest(string url)
        {
            HttpResponseMessage response = await _client.GetAsync(url);

            _client.Dispose();
            _client = null;

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(content);
            }
            else
            {
                throw new Exception($"Could not get the request, {response.StatusCode}");
            }
        }

        public void Dispose()
        {
            if (_client != null)
            {
                _client.Dispose();
                _client = null;
            }
        }
    }
}
