﻿using GNB.Utils.Models;
using System.Collections.Generic;

namespace GNB.Utils.DataClient
{
    public partial class TransactionsClient : DataClient<List<Transaction>>
    {
        public TransactionsClient() : base()
        {
        }
    }
}
