﻿using GNB.Utils.Models;
using System.Collections.Generic;

namespace GNB.Utils.DataClient
{
    public partial class RatesClient : DataClient<List<Rate>>
    {
        public RatesClient() : base()
        {
        }
    }
}
