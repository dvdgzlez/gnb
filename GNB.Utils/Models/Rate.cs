﻿namespace GNB.Utils.Models
{
    public class Rate
    {
        public string from { get; set; }
        public string to { get; set; }
        public string rate { get; set; }
    }
}
