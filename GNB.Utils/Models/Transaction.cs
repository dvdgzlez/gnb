﻿namespace GNB.Utils.Models
{
    public class Transaction
    {
        public string sku { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
    }
}
