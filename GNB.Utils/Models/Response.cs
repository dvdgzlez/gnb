﻿using System.Collections.Generic;

namespace GNB.Utils.Models
{
    public class Response
    {
        public List<Transaction> transactions { get; set; }
        public string amount { get; set; }

        public Response(List<Transaction> newTransactions, double total)
        {
            transactions = newTransactions;
            amount = string.Format("{0:N2}", total);
        }
    }
}
