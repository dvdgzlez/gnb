﻿using GNB.Utils.FileStorage;
using GNB.Utils.Management;
using GNB.Utils.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace GNB.WebService.Controllers
{
    public class RatesController : ApiController
    {
        [HttpGet]
        public List<Rate> Get(string token)
        {
            return new RateManagement(new RateStorage()).Get(token);
        }
    }
}