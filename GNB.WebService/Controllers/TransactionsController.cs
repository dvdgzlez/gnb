﻿using GNB.Utils.FileStorage;
using GNB.Utils.Management;
using GNB.Utils.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace GNB.WebService.Controllers
{
    public class TransactionsController : ApiController
    {
        private TransactionManagement manager;
        public TransactionsController()
        {
            manager = new TransactionManagement(new TransactionsStorage());
        }

        [HttpGet]
        public List<Transaction> Get(string token)
        {
            return manager.Get(token);
        }

        [HttpGet]
        public Response GetBySKU(string token, string sku, string currency = "EUR")
        {
            return manager.GetBySKU(token, sku, currency);
        }
    }
}